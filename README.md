# MakeMake Themes

Themes for MakeMake, the dwarf planet.


## Installing

Just copy paste a theme folder under the *themes* folder of MakeMake.
Each Theme has its own licence specified in the *infos.yml* file.


## Using

Set the MAKEMAKE_THEME in the config.py file.


## Theming

In order to display your Planet, you can use some existing theme or make your own.


### Folder structure

A theme is composed of some files and folders :

    themes/your_theme
    themes/your_theme/infos.yml
    themes/your_theme/index.html
    themes/your_theme/static

*infos.yml* is a file containing informations about theme.
*index.html* is the template used by MakeMake in order to display feeds.
*static* is a folder where you can put any files used by the template (css, images, etc...).


### infos.yml

infos.yml contains informations about the theme:

    name: "Theme's name"
    description: "description of the theme"
    authors: "Authors"
    url: "URL where you can get more informations on this theme (source code, about, contact, ...)"
    licence : "Licence"
    version: "Version"


### HTML templates

Templates use jinja2 templating language. Some specific values can be used:

Informations about Planet:

    mm.title
    mm.link
    mm.logo
    mm.description
    mm.theme
    mm.misc
    mm.last_update

*mm.misc* is specific as it can contains anything in order to extend themes (social networks, related websites, privacy policy, etc...)


Informations about MakeMake:

    mm.makemake.version
    mm.makemake.source
    mm.makemake.author
    mm.makemake.licence


Informations about Themes:

    mm.themes
    mm.themes[n].name
    mm.themes[n].description
    mm.themes[n].authors
    mm.themes[n].url
    mm.themes[n].licence
    mm.themes[n].version


Informations about Sources:

    mm.sources
    mm.sources[n].name
    mm.sources[n].url
    mm.sources[n].link
    mm.sources[n].avatar

*mm.sources[n].url* is the source URL.

*mm.sources[n].link* is the feed URL.


Informations about Feeds and their Entries:

    mm.entries
    mm.entries[n].title
    mm.entries[n].link
    mm.entries[n].date
    mm.entries[n].date_updated
    mm.entries[n].time_elapsed
    mm.entries[n].summary
    mm.entries[n].content
    mm.entries[n].tags
    mm.entries[n].tags[n]
    mm.entries[n].source

*mm.entries[n].source* has the same structure than mm.sources[n].
